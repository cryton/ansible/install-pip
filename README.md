# Ansible role - Install pip
This role provides installation of pip.

## Supported platforms

| Platform | version   |
|----------|-----------|
| Debian   | \>=11     |
| Kali     | \>=2022.1 |

## Requirements

- Root access (specify `become` directive as a global or while invoking the role)
    ```yaml
    become: yes
    ```

## Parameters
No parameters.

## Examples

### Usage
```yaml
  roles:
    - role: install-pip
      become: yes

```

### Inclusion

[https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies)
```yaml
- name: install-pip
  src: https://gitlab.ics.muni.cz/cryton/ansible/install-pip.git
  version: "master"
  scm: git
```
